package com.hackathon.fairprice.retrofit.calls

import android.app.Activity
import android.util.Log
import com.hackathon.fairprice.R
import com.hackathon.fairprice.retrofit.ApiCallClass
import com.hackathon.fairprice.retrofit.ApiInterface
import com.hackathon.fairprice.retrofit.DaggerApiClientComponent
import com.hackathon.fairprice.utilities.AppUtility
import com.hackathon.fairprice.utilities.DialogUtils
import com.hackathon.fairprice.view.listener.BaseListener

class ApiCommon {

    companion object {
        private var apiOauth: ApiCommon? = null
        private var apiInterface: ApiInterface? = null

        fun getInstance(): ApiCommon? {
            if (apiOauth == null) {
                val component = DaggerApiClientComponent.create()
                apiInterface = component.exposeRetrofit().create(ApiInterface::class.java)

                apiOauth = ApiCommon()
            }

            return apiOauth
        }
    }

    interface PropertyCallBack {
        fun onPropertySuccess(baseObject: String)
    }

    fun getProperty(activity: Activity, propertyCallBack: PropertyCallBack) {
        if (!AppUtility.isInternetAvailable(activity)) {
            return
        }

        val apiRequest = apiInterface!!.getProperty()

        ApiCallClass.callApi(true, activity, apiRequest, object : BaseListener.OnWebServiceCompleteListener<String> {
            override fun onWebServiceComplete(baseObject: String) {
                Log.d("Property:", baseObject)
                propertyCallBack.onPropertySuccess(baseObject)
            }

            override fun onWebServiceError(baseObject: String) {
                DialogUtils.singleButtonDialog(activity, activity.getString(R.string.app_name), activity.getString(R.string.error_api), 0)
            }
        })
    }

    interface HouseTypeCallBack {
        fun onHouseTypeSuccess(baseObject: String)
    }

    fun getHouseType(activity: Activity, houseTypeCallback: HouseTypeCallBack) {
        if (!AppUtility.isInternetAvailable(activity)) {
            return
        }

        val apiRequest = apiInterface!!.getHouseType()

        ApiCallClass.callApi(true, activity, apiRequest, object : BaseListener.OnWebServiceCompleteListener<String> {
            override fun onWebServiceComplete(baseObject: String) {
                Log.d("House Type:", baseObject)
                houseTypeCallback.onHouseTypeSuccess(baseObject)
            }

            override fun onWebServiceError(baseObject: String) {
                DialogUtils.singleButtonDialog(activity, activity.getString(R.string.app_name), activity.getString(R.string.error_api), 0)
            }
        })
    }

    interface SearchPropertyCallBack {
        fun onSearchPropertySuccess(baseObject: String)
    }

    fun searchProperty(activity: Activity, state: String, houseType: String, searchPropertyCallBack: SearchPropertyCallBack) {
        if (!AppUtility.isInternetAvailable(activity)) {
            return
        }

        val apiRequest = apiInterface!!.searchProperty(state, houseType)

        ApiCallClass.callApi(true, activity, apiRequest, object : BaseListener.OnWebServiceCompleteListener<String> {
            override fun onWebServiceComplete(baseObject: String) {
                Log.d("Search:", baseObject)
                searchPropertyCallBack.onSearchPropertySuccess(baseObject)
            }

            override fun onWebServiceError(baseObject: String) {
                DialogUtils.singleButtonDialog(activity, activity.getString(R.string.app_name), activity.getString(R.string.error_api), 0)
            }
        })
    }
}