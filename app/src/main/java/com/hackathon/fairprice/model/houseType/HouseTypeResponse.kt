package com.hackathon.fairprice.model.houseType

import com.google.gson.annotations.SerializedName

data class HouseTypeResponse(

	@field:SerializedName("result")
	val result: List<HouseTypeItem>? = null
)