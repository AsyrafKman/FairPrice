package com.hackathon.fairprice.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hackathon.fairprice.R
import com.hackathon.fairprice.ui.activity.LandingActivity
import com.hackathon.fairprice.utilities.DialogUtils
import kotlinx.android.synthetic.main.fragment_update_price.*
import kotlinx.android.synthetic.main.toolbar_landing.*

class UpdatePriceFragment : Fragment(), View.OnClickListener {
    private lateinit var landingActivity: LandingActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_update_price, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    private fun initialize() {
        landingActivity = activity as LandingActivity

        tv_submit.setOnClickListener(this)
        iv_back.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0) {
            iv_back -> {
                activity!!.onBackPressed()
            }
            tv_submit -> {
                DialogUtils.updatePriceDialog(landingActivity, activity!!.getString(R.string.app_name), activity!!.getString(R.string.thankyou), 0)
            }
        }
    }
}