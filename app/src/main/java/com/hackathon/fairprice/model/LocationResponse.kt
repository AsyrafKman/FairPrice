package com.hackathon.fairprice.model

import com.google.gson.annotations.SerializedName

data class LocationResponse(

	@field:SerializedName("location")
	val location: List<LocationItem>? = null
)