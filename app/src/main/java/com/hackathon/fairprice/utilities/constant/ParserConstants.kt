package com.hackathon.fairprice.utilities.constant


class ParserConstants {

    companion object {
        const val TOKEN = "token"
        const val QUANTITY = "quantity"
        const val OPTIONS = "options"
        const val FNAME = "fname"
        const val LNAME = "lname"
        const val NAME = "name"
        const val EMAIL = "email"
        const val PASS = "pass"
        const val PASSWORD = "password"
        const val PHONE = "phone"
        const val DOB = "dob"
        const val SEX = "sex"
        const val PROID = "productid"
        const val CATEGORYID = "categoryid"
        const val PAGE_SIZE = "pagesize"
        const val PAGE = "page"
        const val SORTBY = "sortby"
        const val PRODUCT_ID = "product_id"
        const val MODEL_ID = "model_id"
        const val MODELID = "modelId"
        const val VARIANT_ID = "variant_id"
        const val ADDRESS_ID = "address_id"
        const val ADDRESS = "address"
        const val COUNTRY = "country"
        const val STATE = "state"
        const val POSTCODE = "postcode"
        const val CITY = "city"
        const val isSHIP = "isShip"
        const val isBILL = "isBill"
        const val OLD_PASSWORD = "oldpass"
        const val NEW_PASSWORD = "newpass"
        const val MINPRICE = "minPrice"
        const val MAXPRICE = "maxPrice"
    }

}