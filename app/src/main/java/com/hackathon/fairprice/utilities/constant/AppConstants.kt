package com.hackathon.fairprice.utilities.constant

class AppConstants {

    companion object {
        const val JSON_APPLICATION = "application/json"
    }
}