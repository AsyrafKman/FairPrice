package com.hackathon.fairprice.model.searchProperty

import com.google.gson.annotations.SerializedName

data class PropertyResponse(

	@field:SerializedName("result")
	val result: Result? = null
)