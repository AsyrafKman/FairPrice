package com.hackathon.fairprice.utilities.constant

class PrefConstants {

    companion object {
        const val TOKEN_ID = "token_id"
        const val TOKEN_EXPIRY = "token_expiry"
        const val TOKEN_REFRESH = "token_refresh"
        const val IS_LOGGED_IN = "is_logged_in"
        const val REMEMBERE_ME = "remember_me"
        const val WELCOME_MESSAGE = "welcome_message"
        const val FIRST_NAME = "first_name"
        const val LAST_NAME = "last_name"
        const val FULL_NAME = "customer_name"
        const val CUSTOMER_EMAIL = "customer_email"
        const val CUSTOMER_ID = "customer_id"
        const val CUSTOMER_SID = "customer_sid"
        const val CUSTOMER_PHONE = "customer_phone"
        const val CUSTOMER_BIRTHDAY = "customer_birthday"
        const val CUSTOMER_GENDER = "customer_gender"
        const val CUSTOMER_POINT = "lotalty_points"
        const val CART_COUNT = "cart_count"
        const val CART_ITEMS = "items"
        const val PROFILE_IMAGE = "profile_image"
        const val PAYMENT_METHOD = "payment_method"
        const val PAYMENT_CREDIT_DEBIT = "ccsave"
        const val PAYMENT_BANK = "checkmo"
        const val PAYMENT_CASH_ON_DELIVERY = "cashondelivery"
        const val URL_TO_LOAD = "urltobeloaded"
        const val NOTIFICATIONS = "recNotifications"
        const val PUSHNOTIFICATION = "pushNotifications"
        const val SMSNOTIFICATIONS = "smsNotifications"
        const val NEWSLETTER = "newsletter"
    }
}