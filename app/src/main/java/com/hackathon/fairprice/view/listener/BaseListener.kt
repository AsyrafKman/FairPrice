package com.hackathon.fairprice.view.listener

interface BaseListener {
    interface OnWebServiceCompleteListener<BaseBean> {
        fun onWebServiceComplete(baseObject: BaseBean)
        fun onWebServiceError(baseObject: BaseBean)
    }
}