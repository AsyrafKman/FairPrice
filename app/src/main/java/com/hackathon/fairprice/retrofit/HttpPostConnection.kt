package com.hackathon.fairprice.retrofit

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.WindowManager
import com.hackathon.fairprice.R
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

internal class HttpPostConnection {
    private var mProgressDialog: Dialog? = null
    private var call: Call<ResponseBody>? = null

    private var listener: OnHttpPostConnectionClassCallbacks<String>? = null

    internal interface OnHttpPostConnectionClassCallbacks<String> {
        fun onComplete(result: String)

        fun onError(error: String)
    }

    fun callWebService(showProgressBar: Boolean, context: Context, callType: Call<ResponseBody>, listener: OnHttpPostConnectionClassCallbacks<String>) {
        this.listener = listener
        this.call = callType

        try {
            if (showProgressBar) {
                this.mProgressDialog = Dialog(context)
                this.mProgressDialog!!.setContentView(R.layout.dialog_custom_progress)

                this.mProgressDialog!!.setCancelable(false)
                val window = this.mProgressDialog!!.window
                window?.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
                window?.setGravity(Gravity.CENTER)
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                this.mProgressDialog?.show()
            }
        } catch (ignored: Exception) {
        }

        callHttpCall()
    }

    /**
     * Method to call the API by adding headers to the API Call
     */
    private fun callHttpCall() {
        call!!.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                dimissProgressBar()
                if (response.code() == 200) {
                    successCall(response)
                } else {
                    errorCall("API Issue")
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                dimissProgressBar()
                errorCall(t.localizedMessage)
            }
        })
    }

    private fun successCall(response: Response<ResponseBody>) {
        try {
            listener?.onComplete(response.body()!!.string())
        } catch (e: IOException) {
        }

    }

    private fun errorCall(message: String) {
        listener?.onError(message)
    }

    private fun dimissProgressBar() {
        try {
            if (mProgressDialog!!.isShowing) {
                mProgressDialog?.dismiss()
            }
        } catch (e: Exception) {
            // Handle or log or ignore
        } finally {
            mProgressDialog = null
        }
    }
}