package com.hackathon.fairprice.ui.fragment.authentication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hackathon.fairprice.R
import com.hackathon.fairprice.ui.activity.AuthenticationActivity

class RegisterFragment : Fragment(){
    private lateinit var authenticationActivity: AuthenticationActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_register, container, false)
        initialize(view)
        return view
    }

    private fun initialize(view: View) {
        authenticationActivity = activity as AuthenticationActivity
    }
}