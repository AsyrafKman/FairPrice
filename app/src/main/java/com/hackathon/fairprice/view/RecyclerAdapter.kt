package com.hackathon.fairprice.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hackathon.fairprice.R
import com.hackathon.fairprice.model.searchProperty.ListPropertyItem
import com.hackathon.fairprice.view.listener.RecyclerViewListener

class RecyclerAdapter(private val products: List<ListPropertyItem>, private val listener: RecyclerViewListener) : RecyclerView.Adapter<RecyclerHolder>() {
    override fun onBindViewHolder(holder: RecyclerHolder, p1: Int) {
        holder.bindView(products[p1], listener)
    }


    override fun getItemCount(): Int {
        return products.size
    }

    override fun setHasStableIds(hasStableIds: Boolean) {
        super.setHasStableIds(true)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_propert_list, p0, false)
        return RecyclerHolder(v)
    }
}