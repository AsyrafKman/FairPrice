package com.hackathon.fairprice.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.hackathon.fairprice.R
import com.hackathon.fairprice.ui.fragment.authentication.AuthenticationFragment

class AuthenticationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)

        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.add(R.id.main_continer, AuthenticationFragment())
        transaction.commit()
    }

    fun changeFragmentInBackStack(fragment: Fragment?) {
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction().addToBackStack(null)
        transaction.replace(R.id.main_continer, fragment!!)
        transaction.commit()
    }
}