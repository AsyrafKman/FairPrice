package com.hackathon.fairprice.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.hackathon.fairprice.R
import com.hackathon.fairprice.model.eventbus.PropertyDetailEventBus
import com.hackathon.fairprice.model.searchProperty.ListPropertyItem
import com.hackathon.fairprice.ui.activity.LandingActivity
import com.hackathon.fairprice.view.RecyclerAdapter
import com.hackathon.fairprice.view.listener.RecyclerViewListener
import kotlinx.android.synthetic.main.fragment_property_listing.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class PropertyListFragment : Fragment(), View.OnClickListener, RecyclerViewListener {
    private lateinit var landingActivity: LandingActivity
    private lateinit var propertyList: List<ListPropertyItem>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_property_listing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    private fun initialize() {
        landingActivity = activity as LandingActivity
    }

    override fun onClick(p0: View?) {
        when (p0) {

        }
    }

    override fun onRecyclerClicked(position: Int) {
        landingActivity.openBrowser(propertyList[position].url!!)
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun onEvent(propertyDetailEventBus: PropertyDetailEventBus) {
        propertyList = propertyDetailEventBus.propertyList.shuffled()

        rv_propertyList.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)
        rv_propertyList.adapter = RecyclerAdapter(propertyList, this)
        rv_propertyList.isNestedScrollingEnabled = false
        rv_propertyList.hasFixedSize()
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onStart() {
        EventBus.getDefault().register(this)
        super.onStart()
    }
}