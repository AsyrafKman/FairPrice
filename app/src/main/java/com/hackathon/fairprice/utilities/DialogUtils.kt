package com.hackathon.fairprice.utilities

import android.app.Activity
import android.app.AlertDialog
import android.view.LayoutInflater
import com.hackathon.fairprice.R
import kotlinx.android.synthetic.main.dialog_single_button.view.*

class DialogUtils {

    companion object {

        /**
         * Alert dialog to show information
         *
         * @param activity - To access the context
         * @param title    - Title for the dialog
         * @param message  - Message for the dialog
         */
        fun singleButtonDialog(activity: Activity, title: String, message: String, operation: Int) {
            val layoutInflater = LayoutInflater.from(activity)
            val promptView = layoutInflater.inflate(R.layout.dialog_single_button, null)

            val alertD = AlertDialog.Builder(activity).create()

            promptView.tv_title.text = title
            promptView.tv_dialog_description.text = message
            promptView.ok_btn.setOnClickListener {
                when (operation) {
                    1 -> {
                        alertD.dismiss()
                        activity.finishAffinity()
                    }
                    else -> activity.runOnUiThread {
                        if (!activity.isFinishing) {
                            alertD.dismiss()
                        }
                    }
                }
            }

            alertD.setView(promptView)
            alertD.setCancelable(false)
            activity.runOnUiThread {
                if (!activity.isFinishing) {
                    alertD.show()
                }
            }
        }

        fun updatePriceDialog(activity: Activity, title: String, message: String, operation: Int) {
            val layoutInflater = LayoutInflater.from(activity)
            val promptView = layoutInflater.inflate(R.layout.dialog_thank_you, null)

            val alertD = AlertDialog.Builder(activity).create()

            promptView.tv_title.text = title
            promptView.tv_dialog_description.text = message
            promptView.ok_btn.setOnClickListener {
                when (operation) {
                    1 -> {
                        alertD.dismiss()
                    }
                    else -> activity.runOnUiThread {
                        if (!activity.isFinishing) {
                            alertD.dismiss()
                            activity.onBackPressed()
                        }
                    }
                }
            }

            alertD.setView(promptView)
            alertD.setCancelable(false)
            activity.runOnUiThread {
                if (!activity.isFinishing) {
                    alertD.show()
                }
            }
        }
    }
}